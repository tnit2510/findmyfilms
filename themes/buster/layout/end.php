<!-- footer section-->
<footer class="ht-footer">
	<div class="container">
		<div class="flex-parent-ft">
			<div class="flex-child-ft item1">
				<a href="index-2.html"><img class="logo" src="themes/buster/images/logo1.png" alt=""></a>
				<p>Dự Án 01<br>
				Lớp wd14301</p>
				<p>Teacher : <a href="#">Trần Bá Hộ</a></p>
			</div>
			<div class="flex-child-ft item2">
				<h4>Phim mới</h4>
				<ul>
					<li><a href="#">About</a></li> 
					<li><a href="#">Blockbuster</a></li>
				</ul>
			</div>
			<div class="flex-child-ft item3">
				<h4>Phim lẻ</h4>
				<ul>
					<li><a href="#">Phim hành động</a></li> 
					<li><a href="#">Phim hài</a></li> 
				</ul>
			</div>
			<div class="flex-child-ft item4">
				<h4>Tài khoản</h4>
				<ul>
					<li><a href="#">Bảo Mật</a></li> 
					<li><a href="#">Điều khoản</a></li>	
				</ul>
			</div>
			<div class="flex-child-ft item5">
				<h4>Thành VIên</h4>
				<ul>
					<li><a href="https://www.facebook.com/quyensb1509">Diệp Quốc Quyền</a></li> 
					<li><a href="https://www.facebook.com/tnit.dev">Nguyễn Thành Nhân</a></li> 
					<li><a href="https://www.facebook.com/quocthinhadc">Nguyễn Quốc Thịnh</a></li> 
					<li><a href="https://www.facebook.com/ngocduc.1512">Phạm Ngọc Đức</a></li> 
				</ul>

				<form action="#">
					<input type="text" placeholder="Enter your email...">
				</form>
				<a href="#" class="btn">Subscribe now <i class="ion-ios-arrow-forward"></i></a>
			</div>
		</div>
	</div>
	<div class="ft-copyright">
		<div class="ft-left">
			<p><a target="_blank" href="https://www.templateshub.net">Templates Hub</a></p>
		</div>
		<div class="backtotop">
			<p><a href="#" id="back-to-top">Back to top  <i class="ion-ios-arrow-thin-up"></i></a></p>
		</div>
	</div>
</footer>
<!-- end of footer section-->

<script src="/themes/buster/js/jquery.js"></script>
<script src="/themes/buster/js/plugins.js"></script>
<script src="/themes/buster/js/plugins2.js"></script>
<script src="/themes/buster/js/custom.js"></script>
</body>

<!-- index_light16:29-->
</html>