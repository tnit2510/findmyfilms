

<div class="hero mv-single-hero" style="height: 1600px;">
	<div class="container" style="width: 80%; margin: 0 auto;">
		<div class="row">
			<div class="col-md-12 col-sm-12" style="height: 720px; padding-top: 300px;  margin-top: 100px;">
			<h1> <?= $sfilm['title'] ?></h1>
				<ul class="breadcumb">
					<li class="active"><a href="#">Home</a></li>
					<li> <span class="ion-ios-arrow-right"></span><?=  $sfilm['slug'] ?></li>
				</ul>
				<video id="MY_VIDEO_1" class="video-js vjs-default-skin" controls preload="auto" width="100%" height="100%" poster="MY_VIDEO_POSTER.jpg" data-setup="{}">
					<source src="../upload/<?= $sfilm['link_film'] ?>" type='video/mp4'>
						<source src="MY_VIDEO.webm" type='video/webm'>
							<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
						</video>
						
					</div>
					<div class="col-md-12" style="margin-bottom: 200px; padding-top: 250px;">
							<h1 style="color: #fff;">sơ lược</h1>
							<div style="margin: 0 auto; width: 80% ; height: 300px; position: relative;">
								<p style="position: absolute; padding: 20px; font-weight: bold; font-size: 20px;">
									<?= $sfilm['overview'] ?>
								</p>
							</div>
							<img src="<?php 
							$data = explode("|", $sfilm['cast_images']);
							echo $data[0];
							 ?>" alt="" style="height: 250px;">
						</div>
				</div>
			</div>
		</div>

