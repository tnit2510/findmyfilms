
<main class="content">
	<div class="container-fluid p-0">
		<!-- thêm loại -->
		<div class="col-12 col-xl-12">
			<div class="card">
				<div class="card-header">
					<h5 class="card-title">Sửa nội dung phim số  : #-<?= $data['id'] ?> </h5>
				</div>
				<div class="card-body">
					
					<form method="POST" enctype="multipart/form-data">
						<div class="form-group row">
							<label class="col-form-label col-sm-2 text-sm-right">Tên Phim</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="name" value="<?= $data['title'] ?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-sm-2 text-sm-right">Thể Loại</label>
							<div class="col-sm-10">
								<select class="custom-select" name="cate">
									<?php foreach (list_cate() as $listcat) { ?>
										<option value="<?= $listcat['id'] ?>"><?= $listcat['name'] ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-sm-2 text-sm-right">Summary</label>
							<div class="col-sm-10">
								<textarea  rows="6" class="w-100" name="summary"><?= $data['summary'] ?></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-sm-2 text-sm-right">Link-Film </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="linkfilm" value="<?= $data['link_film'] ?>">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-sm-2 text-sm-right">Link-Trailer </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="linktrailer" value="<?= $data['link_trailer'] ?>">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-sm-10 ml-sm-auto">
								<button type="submit" class="btn btn-success" value="Submit">Submit</button>
							</div>
						</div>
					</form>
				
				</div>
			</div>
		</div>
	</main>
