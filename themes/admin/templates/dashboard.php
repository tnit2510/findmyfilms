
<main class="content">
	<div class="container-fluid p-0">

		<div class="row">
			<div class="col-12 col-lg-12 col-xl-12 d-flex">
				<div class="card flex-fill">
					<div class="card-header">
						<div class="card-actions float-right">
							<div class="dropdown show">
								<a href="#" data-toggle="dropdown" data-display="static">
									<i class="align-middle" data-feather="more-horizontal"></i>
								</a>

								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="#">Action</a>
									<a class="dropdown-item" href="#">Another action</a>
									<a class="dropdown-item" href="#">Something else here</a>
								</div>
							</div>
						</div>
						<h5 class="card-title mb-0">Danh sách Films : </h5>
					</div>
					<table class="table table-striped my-0">
						<thead>
							<tr>
								<th>id</th>
								<th class="d-none d-xl-table-cell">Tên Phim</th>
								<th class="d-none d-xl-table-cell">Thể loại</th>
								<th class="d-none d-xl-table-cell">Slug</th>
								<th class="d-none d-md-table-cell">Link Phim</th>
								<th class="d-none d-md-table-cell">Link Trailer</th>
								<th class="d-none d-md-table-cell">Summary</th>
								<th class="d-none d-md-table-cell">Thumbnail</th>
								<th class="d-none d-md-table-cell">Đăng Lúc</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php if (total()): ?>
								<?php foreach ($list_films as $film): ?>
									<tr>
										<td><?= $film['id'] ?></td>
										<td class="d-none d-xl-table-cell"><?= $film['title'] ?></td>
										<td class="d-none d-xl-table-cell"><?= get_name($film['category_id']) ?></td>
										<td class="d-none d-md-table-cell"><?= $film['slug'] ?></td>
										<td class="d-none d-md-table-cell"><?= $film['link_film'] ?></td>
										<td class="d-none d-md-table-cell"><?= $film['link_trailer'] ?></td>
										<td class="d-none d-md-table-cell"><?= $film['summary'] ?></td>
										<td class="d-none d-md-table-cell"><?= $film['thumbnail'] ?></td>
										<td class="d-none d-xl-table-cell"><?= date("d-m-Y",$film['release_date']) ?></td>

										<td><a href="/admin/edit/<?= $film['id'] ?>" class="btn btn-sm btn-success rounded">Sửa</a>  <a href="/admin/del/<?= $film['id'] ?>" class="btn btn-sm btn-danger rounded">Xóa</a></td>

									</tr>
								<?php endforeach ?>
								<?php else: ?>
									<td><strong>Chưa có phim nào</strong></td>
								<?php endif ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- thêm loại -->
		<div class="col-12 col-xl-12">
			<div class="card">
				<div class="card-header">
					<h5 class="card-title">Thêm Phim : </h5>
					<h6 class="card-subtitle text-muted">Thêm 1 bộ phim.</h6>
				</div>
				<div class="card-body">
					<form method="POST" enctype="multipart/form-data">
						<div class="form-group row">
							<label class="col-form-label col-sm-2 text-sm-right">Tên Phim</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="name" name="name" placeholder="Tên Phim" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-sm-2 text-sm-right">Thể Loại</label>
							<div class="col-sm-10">
								<select class="custom-select" name="cate">
									<?php foreach (list_cate() as $listcat) { ?>
										<option value="<?= $listcat['id'] ?>"><?= $listcat['name'] ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-sm-2 text-sm-right">Summary</label>
							<div class="col-sm-10">
								<textarea  rows="6" class="w-100" name="sumary"></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-sm-2 text-sm-right">Link-Film </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="linkfilm" placeholder="Link- Film">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-sm-2 text-sm-right">Link-Trailer </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="linktrailer" placeholder="Link- Trailer">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-sm-2 text-sm-right">Thumbnail </label>
							<div class="col-sm-10">
								<input type="file" class="validation-file" name="thumb">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-sm-10 ml-sm-auto">
								<button type="submit" class="btn btn-success" value="Submit">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- kết thúc -->
	</main>
