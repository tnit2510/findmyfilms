<main class="content">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12 col-xl-12 d-flex">
				<div class="card flex-fill">
					<div class="card-header">
						<div class="card-actions float-right">
							<div class="dropdown show">
								<a href="#" data-toggle="dropdown" data-display="static">
									<i class="align-middle" data-feather="more-horizontal"></i>
								</a>
							</div>
						</div>
						<h5 class="card-title mb-0">Danh sách Thể loại :</h5>
					</div>
					<table class="table table-striped my-0">
						<thead>
							<tr>
								<th>id</th>
								<th class="">Thể loại</th>
								<th class="">Slug </th>
								<th class="">Đăng Lúc</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach (list_cate() as $listcat) { ?>
								<tr>
									<td><?= $listcat['id'] ?></td>
									<td><?= $listcat['name'] ?></td>
									<td><?= $listcat['slug'] ?></td>
									<td><?=date("d-m-Y",$listcat['created_at'] ) ?></td>
									<td>  
										<a href="/admin/category/edit/<?= $listcat['id'] ?>" class="btn btn-sm btn-success rounded">Chỉnh Sửa</a>
										<a href="/admin/category/del/<?= $listcat['id'] ?>" class="btn btn-sm btn-danger rounded">Xóa</a></td>

									</tr>
								<?php	} ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- thêm loại -->
			<div class="col-12 col-xl-6">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Thêm Loại : </h5>
						<h6 class="card-subtitle text-muted">Thêm 1 thể loại phim.</h6>
					</div>
					<div class="card-body">
						<form method="POST">
							<div class="form-group row">
								<label class="col-form-label col-sm-2 text-sm-right">Thể loại</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="name" name="name" placeholder="Thể loại" required>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-10 ml-sm-auto">
									<input type="submit" name="dang" class="btn btn-success" value="Submit">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- kết thúc -->
		</div>
	</main>
