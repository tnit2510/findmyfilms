<main class="content">
	<div class="container">
			<!-- thêm loại -->
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Chỉnh Sửa Thể Loại : </h5>
						<h6 class="card-subtitle text-muted">Sửa thể loại phim.</h6>
					</div>
					<div class="card-body">
						<form method="POST">
							<div class="form-group row">
								<label class="col-form-label col-sm-2 text-sm-right">Thể loại</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="name" value="<?= $data ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-10 ml-sm-auto">
									<input type="submit" class="btn btn-success" value="Submit">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- kết thúc -->
		</div>
	</main>
