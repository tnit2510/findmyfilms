<?php

/**
 * @package     Findmyfilms
 * @version     0.0.1
 */

require_once('system/bootstrap.php');
require_model('category');
require_model('film');

$title = 'Danh Sách Phim';

$data_cate = get_name_slug($slug);

$total = total_by_id($data_cate['id']);

if ($total) {
    $data_film = list_film($data_cate['id'], $total);
}

require_once('themes/' . THEME . '/layout/head.php');
require_once('themes/' . THEME . '/templates/film/list.php');
require_once('themes/' . THEME . '/layout/end.php');
