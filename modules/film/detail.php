<?php

/**
 * @package     Findmyfilms
 * @version     0.0.1
 */

require_once('system/bootstrap.php');
require_model('category');
require_model('film');

$data = show_film($slug);

require_once('themes/' . THEME . '/layout/head.php');
require_once('themes/' . THEME . '/templates/film/detail.php');
require_once('themes/' . THEME . '/layout/end.php');
