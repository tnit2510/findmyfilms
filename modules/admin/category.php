<?php

/**
 * @package     Findmyfilms
 * @version     0.0.1
 */

$title = 'Quản Lý Thư Mục';

require_once('system/bootstrap.php');
require_model('category');

switch ($act) {
	case 'del_cate':
	del_cate($id);
	redirect('/admin/categories');
	break;
}

if ($request_method == 'POST') {
	$name = $_POST['name'];
	$slug = str_slug($name);
	$time = time();
	add_cate($name,$slug,$time);

	redirect('/admin/categories');
}

require_once('themes/' . THEME_ADMIN . '/layout/head.php');
require_once('themes/' . THEME_ADMIN . '/templates/category.php');
require_once('themes/' . THEME_ADMIN . '/layout/end.php');
