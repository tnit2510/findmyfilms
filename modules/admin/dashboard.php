<?php

/**
 * @package     Findmyfilms
 * @version     0.0.1
 */

$title = 'Quản Lý Film';

require_once('system/bootstrap.php');
require_model('category');
require_model('film');

if ($request_method == 'POST') {
	$name = $_POST['name'];
	$slug = str_slug($name);
	$cate = $_POST['cate'];
	$sumary = $_POST['sumary'];
	$linkfilm = $_POST['linkfilm'];
	$linktrailer = $_POST['linktrailer'];
	$time = time();

	if ($_FILES["thumb"]["tmp_name"] != "") {
		$thumb = time() . '-' . $slug . '.jpg';
		$dir = "uploads/";
		move_uploaded_file($_FILES["thumb"]["tmp_name"],$dir.$thumb);
		upload_film($name,$cate,$slug,$sumary,$linkfilm,$linktrailer,$thumb,$time);
		redirect('/admin');
	}
}

if (total()) {
	$list_films = get_all_film(total());
	switch ($act) {
		case 'del_film':
		del_film($id);
		redirect('/admin');
		break;
	}
}

require_once('themes/' . THEME_ADMIN . '/layout/head.php');
require_once('themes/' . THEME_ADMIN . '/templates/dashboard.php');
require_once('themes/' . THEME_ADMIN . '/layout/end.php');
