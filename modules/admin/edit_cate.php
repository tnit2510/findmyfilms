<?php

/**
 * @package     Findmyfilms
 * @version     0.0.1
 */

$title = 'Quản Lý Thư Mục';

require_once('system/bootstrap.php');
require_model('category');

$data = get_name($id);

if ($request_method == 'POST') {
	$name = isset($_POST['name']) ? $_POST['name'] : $data['name'];;
	$slug = str_slug($name);
	$time = time();
	edit_cate($name,$slug,$time,$id);

	redirect('/admin/categories');
}

require_once('themes/' . THEME_ADMIN . '/layout/head.php');
require_once('themes/' . THEME_ADMIN . '/templates/edit_cate.php');
require_once('themes/' . THEME_ADMIN . '/layout/end.php');
