<?php

/**
 * @package     Findmyfilms
 * @version     0.0.1
 */

$title = 'Quản Lý Film';

require_once('system/bootstrap.php');
require_model('category');
require_model('film');

$data = film_by_id($id);

if ($request_method == 'POST') {
	$name = isset($_POST['name']) ? $_POST['name'] : $data['title'];
	$slug = str_slug($name);
	$slug = isset($_POST['name']) ? str_slug($name) : $data['slug'];
	$cate = isset($_POST['cate']) ? $_POST['cate'] : $data['category_id'];
	$summary = isset($_POST['summary']) ? $_POST['summary'] : $data['summary'];
	$linkfilm = isset($_POST['linkfilm']) ? $_POST['linkfilm'] : $data['linkfilm'];
	$linktrailer = isset($_POST['linktrailer']) ? $_POST['linktrailer'] : $data['linktrailer'];
	$time = time();

	edit_film($name,$cate,$slug,$summary,$linkfilm,$linktrailer,$time,$id);
	redirect('/admin');
}


require_once('themes/' . THEME_ADMIN . '/layout/head.php');
require_once('themes/' . THEME_ADMIN . '/templates/edit_film.php');
require_once('themes/' . THEME_ADMIN . '/layout/end.php');
