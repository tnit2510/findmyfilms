<?php

function total()
{
	return fetchColumn('SELECT count(*) FROM `films`');
}

function total_by_id($id)
{
	return fetchColumn('SELECT count(*) FROM `films` where category_id = ' . $id);
}

function film_by_id($id)
{
	return fetch('SELECT * FROM `films` WHERE `id` = ' .$id. '  LIMIT 1');
}

function get_all_film($total)
{
	return fetchAll('SELECT * FROM `films`' . get_page($total));
}

function list_film($category_id, $total)
{
	return fetchAll('SELECT * FROM `films` WHERE `category_id` = ' . $category_id . ' ORDER BY `id` DESC' . get_page($total));
}

function del_film($id)
{
	return execute('DELETE FROM `films` where `id` = ' .$id. ' LIMIT 1 ');
}

function upload_film($name,$cate,$slug,$sumary,$linkfilm,$linktrailer,$thumb,$release)
{
	$sql =  "INSERT INTO `films`(`title`, `category_id`,`slug`, `summary`, `link_film`, `link_trailer`, `thumbnail`,`release_date`) VALUES ('$name','$cate','$slug','$sumary','$linkfilm','$linktrailer','$thumb','$release')";
	$kq = execute($sql);
	return $kq;
}

function edit_film($name,$cate,$slug,$sumary,$linkfilm,$linktrailer,$release,$id)
{
	$sql =  "UPDATE `films` SET `title` = '$name', `category_id` = '$cate',`slug` = '$slug', `summary` = '$sumary', `link_film` = '$linkfilm', `link_trailer` = '$linktrailer', `release_date` = '$release' where id = '$id' ";
	$kq = execute($sql);
	return $kq;
}
function show_film($slug)
{
	return fetch('SELECT * FROM `films` where `slug`  = "' . $slug . '" LIMIT 1');
}