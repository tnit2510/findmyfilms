<?php

/**
 * @package     Findmyfilms
 * @version     0.0.1
 */

$routes = [
	'/' => 'home/home.php',
	'/admin/categories' => 'admin/category.php',
	'/admin/category/del/(.*)' => 'admin/category.php?act=del_cate&id=$1',
	'/admin/category/edit/(.*)' => 'admin/edit_cate.php?id=$1',
	'/admin/del/(.*)' => 'admin/dashboard.php?act=del_film&id=$1',
	'/admin/edit/(.*)' => 'admin/edit_film.php?id=$1',
	'/admin' => 'admin/dashboard.php',
	'/c/(.*)' => 'film/list.php?slug=$1',
	'/f/(.*)' => 'film/detail.php?slug=$1',
	'/w/(.*)' => 'film/watch.php?slug=$1'
];